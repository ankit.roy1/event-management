import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { UsersEntity } from './entities/users.entity';
import { Event } from 'src/events/entities/events.entity';
import { FavouriteEntity } from './entities/favourite.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UsersEntity, Event, FavouriteEntity])],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
