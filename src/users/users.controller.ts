import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { ERole, UsersService } from './users.service';
import { UserLoginDto } from './dto/user-login.dto';
import { Auth, IAuth } from 'src/utils/auth.decorator';
import { AuthGuard } from 'src/auth/auth.guard';
import { RoleGuard } from 'src/auth/role.guard';
import { PaginateDto } from './../common/dto/paginate.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/my-events')
  @UseGuards(AuthGuard)
  @RoleGuard(ERole.M)
  getMyEvents(@Query() query: PaginateDto, @Auth() auth: IAuth) {
    return this.usersService.getMyEvents(query, auth);
  }

  @Get('/my-favourites')
  @UseGuards(AuthGuard)
  getMyFavouriteEvents(@Query() query: PaginateDto, @Auth() auth: IAuth) {
    return this.usersService.getMyFavouriteEvents(query, auth);
  }

  @Post('/favourite')
  @UseGuards(AuthGuard)
  toggleFavourite(@Body() data, @Auth() auth: IAuth) {
    return this.usersService.toggleFavourite(data, auth);
  }

  @Post('/login')
  login(@Body() userLoginDto: UserLoginDto) {
    return this.usersService.login(userLoginDto);
  }

  @Post('/signup')
  signup(@Body() userLoginDto: UserLoginDto) {
    return this.usersService.signup(userLoginDto);
  }
}
