import * as jwt from 'jsonwebtoken';
import {
  Injectable,
  HttpException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserLoginDto } from './dto/user-login.dto';
import { UsersEntity } from './entities/users.entity';
import { Event } from 'src/events/entities/events.entity';
import { IAuth, IUser } from 'src/utils/auth.decorator';
import { FavouriteEntity } from './entities/favourite.entity';
import { PaginateDto } from './../common/dto/paginate.dto';

export interface data {
  id: number;
  event_id: number;
}

export enum ERole {
  M = 'MANAGER',
  R = 'REGULAR',
}

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly userRepository: Repository<UsersEntity>,
    @InjectRepository(Event)
    private readonly eventRepository: Repository<Event>,
    @InjectRepository(FavouriteEntity)
    private readonly favouriteRepository: Repository<FavouriteEntity>,
  ) {}

  async getMyEvents(query: PaginateDto, auth: IAuth) {
    const user = await auth.user;
    let { page, limit } = query;

    page = page || parseInt(process.env.DEFAULT_PAGE_NUMBER);
    limit = limit || parseInt(process.env.DEFAULT_PAGE_SIZE);

    const [events, count] = await this.eventRepository.findAndCount({
      skip: (page - 1) * limit,
      take: limit,
      where: { user_id: user.id },
      order: { event_date: 'ASC' },
    });

    return {
      events,
      count,
    };
  }

  async toggleFavourite(data: data, auth: IAuth) {
    const user = await auth.user;
    const eventExist = await this.eventRepository.findOneBy({
      id: data.event_id,
    });

    if (!eventExist) {
      throw new HttpException(`Invalid event id #${data.event_id}`, 400);
    }
    console.log(data.event_id);
    let favourite;
    if (auth) {
      favourite = await this.favouriteRepository.findOneBy({
        event_id: data.event_id,
        user,
      });
    }
    console.log('Fav', favourite);

    if (!favourite) {
      const UserFavourite = this.favouriteRepository.create({
        user: user,
        event_id: data.event_id,
        isFavourite: true,
      });

      return this.favouriteRepository.save(UserFavourite);
    } else {
      let updateUserFavourite = await this.favouriteRepository.findOneBy({
        event_id: data.event_id,
        user,
      });

      updateUserFavourite.isFavourite = !updateUserFavourite.isFavourite;
      const res = await this.favouriteRepository.save(updateUserFavourite);
      return res;
    }
  }

  async getMyFavouriteEvents(paginateDto: PaginateDto, auth: IAuth) {
    const user: IUser = await auth.user;
    let { page, limit } = paginateDto;

    page = page || parseInt(process.env.DEFAULT_PAGE_NUMBER);
    limit = limit || parseInt(process.env.DEFAULT_PAGE_SIZE);

    const [favourites, count] = await this.favouriteRepository.findAndCount({
      skip: (page - 1) * limit,
      take: limit,
      where: { user, isFavourite: true },
      order: { id: 'ASC' },
    });

    return {
      favourites,
      count,
    };
  }

  async login(userLoginDto: UserLoginDto) {
    const user = await this.userRepository.findOneBy({
      email: userLoginDto.email,
    });

    const isMatch = await bcrypt.compare(userLoginDto.password, user.password);

    if (user && isMatch) {
      const token = jwt.sign({ id: user.id }, 'secrettobekeptindotenv', {
        expiresIn: '30d',
      });

      return {
        ...user,
        password: undefined,
        jwt: token,
      };
    } else {
      throw new HttpException('Invalid email or password', 400);
    }
  }

  verifyToken(request) {
    try {
      let token;
      if (
        request.headers.authorization &&
        request.headers.authorization.startsWith('Bearer')
      ) {
        token = request.headers.authorization.split(' ')[1];
      }

      const decoded: any = jwt.verify(token, 'secrettobekeptindotenv');
      const userId = decoded.id;

      const user = UsersEntity.findOneBy({ id: userId });

      if (user) {
        request.user = user;
      }
      return user;
    } catch (error) {
      return null;
    }
  }

  async signup(userLoginDto: UserLoginDto) {
    const { email, password } = userLoginDto;

    const emailExist = await this.userRepository.findOneBy({ email });

    if (emailExist) {
      throw new HttpException('User with that email already exists', 400);
    }

    const saltOrRounds = 10;
    const hash = await bcrypt.hash(password, saltOrRounds);

    const user = this.userRepository.create({
      email,
      password: hash,
      role: 'MANAGER',
    });

    return this.userRepository.save(user);
  }
}
