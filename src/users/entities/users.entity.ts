import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  OneToMany,
} from 'typeorm';
import { FavouriteEntity } from './favourite.entity';

@Entity()
export class UsersEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  role: string;

  @OneToMany((_type) => FavouriteEntity, (favourite) => favourite.user, {
    eager: true,
  })
  favourites: FavouriteEntity[];
}
