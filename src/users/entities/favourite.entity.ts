import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { UsersEntity } from 'src/users/entities/users.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class FavouriteEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  event_id: number;

  @Column()
  isFavourite: boolean;

  @ManyToOne((_type) => UsersEntity, (user) => user.favourites, {
    eager: false,
  })
  @Exclude({ toPlainOnly: true })
  user: UsersEntity;
}
