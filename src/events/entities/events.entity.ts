import { FavouriteEntity } from 'src/users/entities/favourite.entity';
import {
  Entity,
  Column,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { TagEntity } from './tag.entity';

@Entity()
export class Event {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  address: string;

  @Column()
  category: string;

  @Column({ type: 'date' })
  event_date: Date;

  @JoinTable()
  @ManyToMany(() => TagEntity, (tag) => tag.events, { cascade: true })
  tags: TagEntity[];

  @Column()
  isVirtual: boolean;
}
