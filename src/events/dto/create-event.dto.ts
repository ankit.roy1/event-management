import { IsString, IsBoolean } from 'class-validator';

export class CreateEventDto {
  @IsString()
  readonly title: string;

  @IsString()
  readonly description: string;

  @IsString()
  readonly address: string;

  @IsString()
  readonly category: string;

  @IsString({ each: true })
  readonly tags: string[];

  @IsString()
  readonly event_date: string;

  @IsBoolean()
  readonly isVirtual: boolean;
}
