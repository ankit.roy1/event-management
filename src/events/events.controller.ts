import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Patch,
  Query,
  UseGuards,
  Req,
} from '@nestjs/common';
import { CreateEventDto } from './dto/create-event.dto';
import { EventsService } from './events.service';
import { AuthGuard } from './../auth/auth.guard';
import { RoleGuard } from 'src/auth/role.guard';
import { ERole } from 'src/users/users.service';
import { Auth, IAuth } from 'src/utils/auth.decorator';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { UsersService } from './../users/users.service';

@Controller('events')
export class EventsController {
  constructor(
    private readonly eventsService: EventsService,
    private readonly usersService: UsersService,
  ) {}
  @Get('/')
  async getEvents(@Query() query: PaginationQueryDto, @Req() req) {
    let user;
    if (req.headers.authorization) {
      user = await this.usersService.verifyToken(req);
    }
    return this.eventsService.findAll(query, user);
  }

  @Post()
  @UseGuards(AuthGuard)
  @RoleGuard(ERole.M)
  createEvent(@Body() createEventDto: CreateEventDto, @Auth() auth: IAuth) {
    return this.eventsService.create(createEventDto, auth);
  }

  @UseGuards(AuthGuard)
  @RoleGuard(ERole.M)
  @Delete('/:id')
  deleteEvent(@Param('id') id: number, @Auth() auth: IAuth) {
    return this.eventsService.remove(id, auth);
  }

  @Patch('/:id')
  updateFavouriteStatus(@Param('id') id: string, @Query() query) {
    const { page, limit } = query;
    return `this updates event id ${id} a favourite status`;
  }

  @Get('/categories')
  async getAllCategories() {
    return this.eventsService.getAllCategories();
  }
}
