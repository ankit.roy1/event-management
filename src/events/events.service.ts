import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like, MoreThan, LessThan } from 'typeorm';
import * as moment from 'moment';
import { Event } from './entities/events.entity';
import { CreateEventDto } from './dto/create-event.dto';
import { TagEntity } from './entities/tag.entity';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { FavouriteEntity } from './../users/entities/favourite.entity';
import { IAuth } from 'src/utils/auth.decorator';

@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event)
    private readonly eventRepository: Repository<Event>,
    @InjectRepository(TagEntity)
    private readonly tagRepository: Repository<TagEntity>,
    @InjectRepository(FavouriteEntity)
    private readonly favouriteRepository: Repository<FavouriteEntity>,
  ) {}

  async findAll(query: PaginationQueryDto, user) {
    let { limit, page } = query;
    let filterQuery = { ...query, limit: undefined, page: undefined };

    page = page || parseInt(process.env.DEFAULT_PAGE_NUMBER);
    limit = limit || parseInt(process.env.DEFAULT_PAGE_SIZE);

    const filter = this.filterObject(filterQuery);

    const [events, count] = await this.eventRepository.findAndCount({
      relations: ['tags'],
      skip: (page - 1) * limit,
      take: limit,
      where: { ...filter },
      order: { event_date: 'ASC' },
    });

    // console.log(user);  Missing join favourites
    // if (!user) {
    //   return {
    //     events,
    //     count,
    //   };
    // }

    // const results = events.map((event) => {
    //   const isFavourite = this.favouriteRepository.findBy({
    //     user: user,
    //     // isFavourite: true,
    //     event_id: event.id,
    //   });
    //   return isFavourite;
    // });

    // console.log('what', results);
    // Promise.all(results)
    //   .then((res) => {
    //     console.log('fav', res);
    //   })
    //   .catch((err) => {
    //     console.log('error');
    //   });

    return {
      events,
      count,
    };
  }

  async create(createEventDto: CreateEventDto, auth) {
    const dateObj = new Date(createEventDto.event_date);
    const today = moment();

    if (moment(dateObj).isBefore(today)) {
      throw new HttpException(`Event date cannot be a past date`, 400);
    }

    const tags = await Promise.all(
      createEventDto.tags.map((name) => this.preloadTagsByName(name)),
    );

    const user = await auth.user;

    const event = this.eventRepository.create({
      ...createEventDto,
      event_date: dateObj,
      user_id: user.id,
      tags,
    });

    return this.eventRepository.save(event);
  }

  async remove(id: number, auth: IAuth) {
    const user = await auth.user;
    const event = await this.eventRepository.findOneBy({
      id,
      user_id: user.id,
    });

    if (!event) {
      throw new HttpException(`Invalid Event id #${id}`, 400);
    }

    const today = moment();
    if (moment(event.event_date).isBefore(today)) {
      throw new HttpException(`Cannot delete a past event`, 400);
    }

    return this.eventRepository.remove(event);
  }

  private async preloadTagsByName(name: string): Promise<TagEntity> {
    const existingTag = await this.tagRepository.findOneBy({ name });

    if (existingTag) {
      return existingTag;
    }

    return this.tagRepository.create({ name });
  }

  async getAllCategories() {
    const categories = await this.eventRepository
      .createQueryBuilder('event')
      .select('category')
      .distinct(true)
      .getRawMany();
    return categories;
  }

  private filterObject(obj) {
    let filteredObj = {};
    for (let key in obj) {
      if (
        obj[key] === undefined ||
        (typeof obj[key] === 'string' && obj[key].trim() === '')
      ) {
        delete obj[key];
      }
    }

    for (let key in obj) {
      if (typeof obj[key] === 'string') {
        filteredObj[key] = Like(`%${obj[key]}%`);
      } else if (typeof obj[key] === 'boolean') {
        filteredObj[key] = obj[key];
      } else {
        if (key.includes('before')) {
          const date = moment(obj[key]).format(`YYYY-MM-DD`);
          filteredObj['event_date'] = LessThan(`${date}`);
        } else {
          const date = moment(obj[key]).format(`YYYY-MM-DD`);
          filteredObj['event_date'] = MoreThan(`${date}`);
        }
      }
    }
    console.log(filteredObj);
    return filteredObj;
  }
}
