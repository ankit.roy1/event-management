import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';
import { Event } from './entities/events.entity';
import { TagEntity } from './entities/tag.entity';
import { FavouriteEntity } from 'src/users/entities/favourite.entity';
import { UsersModule } from './../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Event, TagEntity, FavouriteEntity]),
    UsersModule,
  ],
  controllers: [EventsController],
  providers: [EventsService],
})
export class EventsModule {}
