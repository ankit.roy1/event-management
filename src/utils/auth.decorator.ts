import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IsNumberOptions } from 'class-validator';

export interface IUser {
  id: number;
  email: string;
  password: string;
  role: string;
}

export interface IAuth {
  user: IUser;
  token: string;
}

export const Auth = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): IAuth => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    const token = request.headers.authorization.split(' ')[1];
    return { user, token };
  },
);
