import {
  IsBoolean,
  IsDate,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
export class PaginationQueryDto {
  @IsOptional()
  @IsPositive()
  page: number;

  @IsPositive()
  @IsOptional()
  limit: number;

  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  address: string;

  @IsBoolean()
  @IsOptional()
  isVirtual: boolean;

  @IsString()
  @IsOptional()
  category: string;

  @IsDate()
  @IsOptional()
  beforeDate: Date;

  @IsDate()
  @IsOptional()
  afterDate: Date;
}
