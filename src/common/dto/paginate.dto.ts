import { IsOptional, IsPositive } from 'class-validator';
export class PaginateDto {
  @IsOptional()
  @IsPositive()
  page: number;

  @IsPositive()
  @IsOptional()
  limit: number;
}
